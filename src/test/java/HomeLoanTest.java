import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.WebDriver;
import page.ContactForm;
import page.Driver;
import page.HomeLoanPage;
import page.HomePage;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class HomeLoanTest {

    private WebDriver webDriver;
    private HomePage homePage;
    private HomeLoanPage homeLoanPage;
    private ContactForm contactForm;

    @BeforeAll
    public void beforeAll() {
        webDriver = new Driver().getDriver();
        homePage = new HomePage(webDriver);
        homeLoanPage = new HomeLoanPage(webDriver);
        contactForm = new ContactForm(webDriver);
    }

    @Test
    public void loginTest() throws InterruptedException {
        homePage.goToWebPage("https://www.nab.com.au/");
        homePage.clickEnquireNow();
        homeLoanPage.clickRequestACallBack();
        homeLoanPage.selectHomeLoanType("New home loans");
        homeLoanPage.clickNextButton();
        homeLoanPage.selectHomeLoanTopic("Buy new property");
        homeLoanPage.clickNextInNewHomeLoanPage();
        contactForm.switchTab();
        contactForm.fillCallBackForm();
        contactForm.assertYourRequest();
    }

    @AfterAll
    public void afterAll() {
        webDriver.quit();
    }
}
