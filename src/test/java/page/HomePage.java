package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    private WebDriver webDriver;
    private Util util;

    public HomePage(WebDriver driver) {
        webDriver = driver;
        util = new Util(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//a[starts-with(@aria-label, 'Enquire now about')]")
    private WebElement enquireNow;

    public void goToWebPage(String url) {
        webDriver.navigate().to(url);
    }

    public void clickEnquireNow() {
        util.clickAnElement(enquireNow);
    }
}
