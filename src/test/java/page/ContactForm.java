package page;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;

public class ContactForm {

    private WebDriver webDriver;
    private Util util;
    private JavascriptExecutor javascriptExecutor;

    public ContactForm(WebDriver driver) {
        webDriver = driver;
        util = new Util(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//input[@label='First name']")
    private WebElement firstName;

    @FindBy(xpath = "//input[@label='Last name']")
    private WebElement lastName;

    @FindBy(xpath = "//input[@label='Phone number']")
    private WebElement phoneNumber;

    @FindBy(xpath = "//input[@label='Email']")
    private WebElement email;

    @FindBy(xpath = "//span[contains(text(),'Submit')]")
    private WebElement submitButton;

    @FindBy(xpath = "//div[@id='page-Page1-aboutYou-state']/div")
    private WebElement selectState;

    @FindBy(xpath = "//span[contains(text(),'No')]")
    private WebElement notExistingCustomer;

    @FindBy(xpath = "//*[contains(text(),'NSW')]")
    private WebElement nswState;

    @FindBy(xpath = "//h1[contains(text(),'RECEIVED YOUR REQUEST')]")
    private WebElement receivedRequest;

    // We can use Faker library and define in separate class for data creation
    public void fillCallBackForm() {
        util.waitForCondition("visible", notExistingCustomer);
        util.waitForCondition("visible", firstName);
        util.clickAnElement(notExistingCustomer);
        util.enterText(firstName, "neter");
        util.enterText(lastName, "nagra");

        util.enterText(phoneNumber, "0410010824");
        util.enterText(email, "neternagra@gmail.com");
        util.waitForCondition("visible", selectState);
        util.moveToElementAndClick(selectState);

        util.waitForCondition("visible", nswState);
        util.moveToElementAndClick(nswState);
        util.clickAnElement(submitButton);
    }

    public void assertYourRequest() {
        util.waitForCondition("visible", receivedRequest);
    }

    public void switchTab() throws InterruptedException {
        ArrayList<String> tabList = new ArrayList<String> (webDriver.getWindowHandles());
        int i = 0;
        while (tabList.size() == 0) {
            i = i + 1;
            if (i == 15) {
                break;
            }
        }
        webDriver.switchTo().window(tabList.get(1));
    }
}
