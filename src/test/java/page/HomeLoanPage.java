package page;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomeLoanPage {

    private WebDriver webDriver;
    private Util util;
    private JavascriptExecutor javascriptExecutor;

    public HomeLoanPage(WebDriver driver) {
        webDriver = driver;
        util = new Util(driver);
        javascriptExecutor = (JavascriptExecutor) driver;
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//a[starts-with(@aria-label, 'Call me back')]")
    private WebElement callMeBack;

    @FindBy(xpath = "//span[contains(text(),'View all home loans')]")
    private WebElement viewAllHomeLoans;

    public void clickRequestACallBack() {
        util.clickAnElement(callMeBack);
    }

    public void selectHomeLoanType(String type) {
        if (type.equalsIgnoreCase("New home loans")) {
            String string = "return document.querySelector('#contact-form-shadow-root').shadowRoot.querySelector('#myRadioButtons-0 > label')";
            clickElementByJavaScript(string);
        }
    }

    public void clickNextButton() {
        String string = "return document.querySelector('#contact-form-shadow-root').shadowRoot.querySelector('#main-container > div > div.sc-ifAKCX.Col__StyledCol-o7bhp7-0.ibULtI > section > div.sc-bdVaJa.iAQrVS > button > div > span')";
        clickElementByJavaScript(string);
    }

    public void selectHomeLoanTopic(String topic) {
        if (topic.equalsIgnoreCase("Buy new property")) {
            String string = "return document.querySelector('#contact-form-shadow-root').shadowRoot.querySelector('#myRadioButtons-0 > label')";
            clickElementByJavaScript(string);
        }
    }

    public void clickNextInNewHomeLoanPage() {
        String string = "return document.querySelector('#contact-form-shadow-root').shadowRoot.querySelector('#main-container > div > div.sc-ifAKCX.Col__StyledCol-o7bhp7-0.ibULtI > section > div.sc-bdVaJa.iAQrVS > button.Buttonstyle__StyledButton-sc-1vu4swu-3.cchfek > div')";
        clickElementByJavaScript(string);
    }

    public void clickElementByJavaScript(String path) {
        WebElement webElement = (WebElement) javascriptExecutor.executeScript(path);
        util.clickAnElement(webElement);
    }
}
