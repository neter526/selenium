package page;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;

public class Util {

    private WebDriver webDriver;
    private Actions actions;

    public Util(WebDriver driver) {
        webDriver = driver;
        actions = new Actions(webDriver);
        PageFactory.initElements(webDriver, this);
    }

    public void enterText(WebElement element, String text) {
        waitForCondition("visible", element);
        element.sendKeys(text);
    }

    public void moveToElementAndClick(WebElement element) {
        waitForCondition("visible", element);
        actions.moveToElement(element).perform();
        actions.click(element).perform();
    }

    public void clickAnElement(WebElement element) {
        waitForCondition("visible", element);
        waitForCondition("click", element);
        JavascriptExecutor jse = (JavascriptExecutor)webDriver;
        jse.executeScript("arguments[0].click();", element);
    }

    public void waitForCondition(String typeOfWait, WebElement element) {
        try {
            Wait<WebDriver> wait = getWebDriverWait(40);
            switch (typeOfWait) {
                case "click":
                    wait.until(ExpectedConditions.elementToBeClickable(element));
                    break;
                case "visible":
                    wait.until(ExpectedConditions.visibilityOf(element));
                    break;
                default:
                    wait.until(ExpectedConditions.jsReturnsValue("return document.readyState==\"complete\";"));
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    public Wait<WebDriver> getWebDriverWait(long durationOfWait) {
        Wait<WebDriver> wait = new FluentWait<>(webDriver).withTimeout(Duration.ofSeconds(durationOfWait))
                .pollingEvery(Duration.ofSeconds(5)).ignoring(NoSuchElementException.class);

        return wait;
    }
}